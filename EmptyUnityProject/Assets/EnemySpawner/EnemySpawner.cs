﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour
{
    private float StartSpawnTime;
    public float SpawnInterval = 10.0f;
    public GameObject Enemy;

	void Start()
    {
        StartSpawnTime = Time.time;
	}
	
	void Update()
    {
        if (Time.time - StartSpawnTime >
            SpawnInterval)
        {
            GameObject.Instantiate(
                Enemy,
                transform.position,
                transform.rotation
            );
            StartSpawnTime = Time.time;
        }
	}
}
