﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
	public float Speed = 2.0f;
	public Camera Camera;
    public Text HPText;

    private float HealthPoints = 100;
    private NavMeshAgent NavMeshAgent;
	private Vector3 CameraRelativeOffset;

	void Start()
	{
        HPText.text = HealthPoints.ToString();
		NavMeshAgent = GetComponent<NavMeshAgent>();

		CameraRelativeOffset =
			Camera.transform.position -
			transform.position;
	}
	
	void Update()
	{
		GetComponent<Animator>().SetFloat(
			"Speed",
			NavMeshAgent.velocity.magnitude
		);

		Camera.transform.position =
			transform.position + CameraRelativeOffset;
        
		if (Input.GetMouseButtonDown(1))
		{
			Ray ray = Camera.main.ScreenPointToRay(
				Input.mousePosition
			);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 100))
			{
				NavMeshAgent.SetDestination(hit.point);
			}
		}

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(
                    Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                EnemyController enemy =
                    hit.transform.gameObject.
                    GetComponent<EnemyController>();
                if (enemy != null)
                {
                    enemy.Damaged();
                }
            }
        }
	}

    public void Damaged()
    {
        HealthPoints -= 0.5f;
        HPText.text = 
            Mathf.Round(HealthPoints).ToString();
        if (HealthPoints <= 0)
        {
            GameObject.Destroy(gameObject);
        }
    }
}
