﻿using UnityEngine;
using System.Collections;

public class DoorController : MonoBehaviour
{
	private Animator Animator;
	public GameObject DoorTrigger;
	public float DoorTriggerDistance = 3.0f;

	bool Open = false;

	// Use this for initialization
	void Start()
	{
		Animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update()
	{
		var gameObjects = 
			GameObject.FindGameObjectsWithTag("Player");
		bool isPlayerNear = false;
		foreach (var gameObject in gameObjects)
		{
			if (Vector3.Distance(
				gameObject.transform.position,
				DoorTrigger.transform.position
			) < DoorTriggerDistance)
			{
				isPlayerNear = true;
				break;
			}
		}
		if (!Open && isPlayerNear)
		{
			Open = true;
			Animator.SetTrigger ("Open");
		}
		else if (Open && !isPlayerNear)
		{
			Open = false;
			Animator.SetTrigger ("Close");
		}
	}
}
